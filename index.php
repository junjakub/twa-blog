<?php

include 'api.php';

$trainings = getTrainings();


?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>

<body>

    <!-- Navigation -->


    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4">Treninky
          </h1>

          <!-- Blog Post -->


                      <?php
                      session_start();

                      foreach ($trainings as $row) {
                        echo $row["nazev"];

                        $registered = getNumberOfRegistered(intval($row['id']));

                        echo ", " . $registered . " registrovano";

                        echo "<a href=\"register.php?id={$row['id']}\">registrovat</a>, ";

                        echo "<a href=\"unregister.php?id={$row['id']}\">odregistrovat</a><br>";

                        $people = getRegisteredPeople(intval($row['id']));

                        echo "<ul>";
                        foreach ($people as $abc) {
                          echo "<li>{$abc['username']}</li>";
                        }
                        echo "</ul>"; 

                      }




                  ?>

      </div>


        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

          <!-- Search Widget -->
          <div class="card my-4">
            <h5 class="card-header">Prihlaseni</h5>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-12">
                  <ul class="list-unstyled mb-0">
                    <?php
                    session_start();
                    if ($_SESSION["logged"] == "yes") {
                      echo "<li>jste prihlasen jako <b>{$_SESSION['username']}</b>!</li>";
                      echo "<li><a href='logout.php'>odhlaseni</a></li>";
                    } else {
                      echo "<li><b>nejste prihlasen!</b></li>";
                      echo "
                      <form action='auth.php' method='post'>
                      <div class='form-group'>
                        <label>Jmeno:</label>
                        <input class='form-control' type='text' name='login'><br>
                        </div>
                        <button type='submit' class='btn btn-primary'>odeslat</button>
                      </form>";
                    }
                    ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>



         


        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->



    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>








</html>
