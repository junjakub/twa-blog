<?php
include 'api.php';
session_start();
$id = $_GET['id'];

if (!isset($_SESSION["username"])) {
  header('Location: index.php');
  die();
  }

registerUserToTraining($_SESSION["username"], $id);

header('Location: index.php');
die();
