<?php

function getNumberOfRegistered($trainingId) {
  $pdo = new PDO('sqlite:db.sqlite3');
  $sql = "SELECT count(*) FROM treninky INNER JOIN registrace ON treninky.id=registrace.id_treninku WHERE treninky.id = {$trainingId} ";
  $result = $pdo->prepare($sql); 
  $result->execute(); 
  $number_of_rows = $result->fetchColumn(); 
  return $number_of_rows;
}

function isUserLoggedToTraining($username, $trainingId) {
  $pdo = new PDO('sqlite:db.sqlite3');
$sql = "SELECT count(*) FROM treninky INNER JOIN registrace ON treninky.id=registrace.id_treninku WHERE treninky.id={$trainingId} AND username='{$username}'";
  $result = $pdo->prepare($sql); 
  $result->execute(); 
  $number_of_rows = $result->fetchColumn(); 
  return ($number_of_rows != 0);
}

function getTrainings() {
  $pdo = new PDO('sqlite:db.sqlite3');
  $stmt = $pdo->query('SELECT * FROM treninky');
  $results = [];
  while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
    $results[]=$row;
  }
  
  return $results;
}

function registerUserToTraining($username, $trainingId) {

  $pdo = new PDO('sqlite:db.sqlite3');
$sql = "INSERT INTO registrace(id_treninku, username) VALUES ({$trainingId}, '{$username}')";
  $result = $pdo->prepare($sql); 
  $result->execute(); 

}

function unregisterUserFromTraining($username, $trainingId) {
  $pdo = new PDO('sqlite:db.sqlite3');
$sql = "DELETE FROM registrace WHERE id_treninku = {$trainingId} AND username = '{$username}'";
  $result = $pdo->prepare($sql); 
  $result->execute(); 

}

function getRegisteredPeople($trainingId) {
  $pdo = new PDO('sqlite:db.sqlite3');
  $stmt = $pdo->query("SELECT * FROM treninky INNER JOIN registrace ON treninky.id=registrace.id_treninku WHERE treninky.id = {$trainingId}");
  $results = [];
  while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
    $results[]=$row;
  }
  
  return $results;
}